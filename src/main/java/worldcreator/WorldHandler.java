package worldcreator;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.bukkit.World;

public class WorldHandler {  
    private static Set<World> acw = new HashSet<>();
    private static Set<World> selfcontained = new HashSet<>();
    private static List<World> playable = new ArrayList<>();
    
    public static void addSelfContained(World w) {
        selfcontained.add(w);
    }
    
    public static boolean isSelfContained(World w) {
        return selfcontained.contains(w);
    }
    
    public static void allowCrossWorld(World w) {
        playable.add(w);
        acw.add(w);
    }
    
    public static boolean isAllowedCrossWorld(World w) {
        return acw.contains(w);
    }
    
    public static List<World> getPlayable() {
        return playable;
    }

}
