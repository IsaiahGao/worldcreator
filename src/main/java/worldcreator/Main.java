package worldcreator;

import org.bukkit.Bukkit;
import org.bukkit.Difficulty;
import org.bukkit.GameRule;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.WorldCreator;
import org.bukkit.WorldType;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.entity.Arrow;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EvokerFangs;
import org.bukkit.entity.Monster;
import org.bukkit.entity.Player;
import org.bukkit.entity.Projectile;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityExplodeEvent;
import org.bukkit.event.entity.EntityTargetEvent;
import org.bukkit.plugin.java.JavaPlugin;

import utilities.Utils;
import utilities.WorldAccess;

public class Main extends JavaPlugin implements Listener {
    
    public static final BlockFace[] octahedron = {BlockFace.NORTH, BlockFace.EAST, BlockFace.SOUTH, BlockFace.WEST, BlockFace.UP, BlockFace.DOWN};

    @Override
    public void onEnable() {
        createWorlds();
        addAllowed();
        this.getServer().getPluginManager().registerEvents(this, this);

        /*try {
        	WorldServer world = ((CraftWorld) Bukkit.getServer().getWorld("world")).getHandle();
        	GeneratorSettings generatorSettings = world.o().getChunkGenerator().getSettings();
        	
        	Field f = GeneratorSettingsDefault.class.getDeclaredField("p");
        	f.setAccessible(true);
        	f.set(generatorSettings, 40);
        	
        	Field f2 = GeneratorSettingsDefault.class.getDeclaredField("q");
        	f2.setAccessible(true);
        	f2.set(generatorSettings, 20);

        	Field a = GeneratorSettingsDefault.class.getDeclaredField("p");
        	a.setAccessible(true);

        	Field b = GeneratorSettingsDefault.class.getDeclaredField("q");
        	b.setAccessible(true);
        	for (int i = 0; i < 50; i++)
        	System.out.println("modified p is " + a.getInt(generatorSettings));
        	for (int i = 0; i < 50; i++)
        	System.out.println("modified q is " + b.getInt(generatorSettings));
        } catch (Exception e) {
        	e.printStackTrace();
        }*/
    }
    
    private static World misc;
    
    private static void addAllowed() {
        WorldHandler.allowCrossWorld(Bukkit.getWorld("world"));
        WorldHandler.allowCrossWorld(Bukkit.getWorld("world_nether"));
        WorldHandler.allowCrossWorld(Bukkit.getWorld("world_the_end"));
    }
    
    private static void createWorlds() {
        WorldCreator wc = new WorldCreator("arena");
        wc = wc.generator(new CleanroomChunkGenerator("."));
        World arena = Bukkit.getServer().createWorld(wc);
        arena.setGameRule(GameRule.KEEP_INVENTORY, true);
        
        WorldCreator wcc = new WorldCreator("creative");
        wcc.type(WorldType.FLAT);
        wcc.generateStructures(false);
        World creative = Bukkit.getServer().createWorld(wcc);
        creative.setGameRule(GameRule.DO_MOB_LOOT, false);
        creative.setGameRule(GameRule.DO_MOB_SPAWNING, false);
        creative.setGameRule(GameRule.MOB_GRIEFING, false);
        
        WorldCreator wccc = new WorldCreator("shadowrealm");
        //wccc = wccc.generator("OpenTerrainGenerator");
        wccc = wccc.generator(new CleanroomChunkGenerator("."));
        World shadowrealm = Bukkit.getServer().createWorld(wccc);
        shadowrealm.setGameRule(GameRule.KEEP_INVENTORY, true);
        shadowrealm.setGameRule(GameRule.DO_DAYLIGHT_CYCLE, false);
        shadowrealm.setDifficulty(Difficulty.HARD);
        shadowrealm.setPVP(false);
        shadowrealm.getWorldBorder().setSize(30000);
        shadowrealm.setTime(18000);
        WorldHandler.allowCrossWorld(shadowrealm);
        WorldHandler.addSelfContained(shadowrealm);
        
        WorldCreator wc4 = new WorldCreator("misc");
        wc4.type(WorldType.FLAT);
        wc4.generateStructures(false);
        misc = Bukkit.getServer().createWorld(wc4);
        misc.setGameRule(GameRule.KEEP_INVENTORY, true);
        misc.setGameRule(GameRule.DO_MOB_SPAWNING, false);
        misc.setGameRule(GameRule.MOB_GRIEFING, false);
        misc.setDifficulty(Difficulty.HARD);
        misc.setPVP(false);
        misc.setGameRule(GameRule.DO_DAYLIGHT_CYCLE, false);
        misc.setGameRule(GameRule.DO_TILE_DROPS, false);
        misc.setGameRule(GameRule.DO_MOB_LOOT, false);
        misc.setTime(18000);
        WorldHandler.allowCrossWorld(misc);
        WorldHandler.addSelfContained(misc);
        
        WorldCreator wcd = new WorldCreator("dragonlobby");
        wcd = wcd.generator(new CleanroomChunkGenerator("."));
        World dragonlobby = Bukkit.getServer().createWorld(wcd);
        dragonlobby.setGameRule(GameRule.KEEP_INVENTORY, true);
        dragonlobby.setPVP(false);
    }
    
    @EventHandler(priority = EventPriority.LOWEST)
    public void blockBreak(BlockBreakEvent e) {
        if (! WorldAccess.canPlayerBuild(e.getBlock().getWorld())) {
            if (e.getPlayer().isOp())
                return;
            

            if (e.getBlock().getWorld() == misc) {
                if (Utils.isGrass(e.getBlock().getType())) {
                    e.getBlock().setType(Material.AIR);
                    e.setCancelled(true);
                    return;
                }
                
                if (e.getBlock().getType() == Material.COBWEB) {
                    for (BlockFace b : octahedron) {
                        Block rel = e.getBlock().getRelative(b);
                        if (rel.getType() == e.getBlock().getType()) {
                            rel.setType(Material.AIR);
                        }
                    }
                    e.getBlock().setType(Material.AIR);
                    e.setCancelled(true);
                    return;
                }
            }
            e.setCancelled(true);
        }
    }
    
    @EventHandler
    public void explode(EntityExplodeEvent e) {
        Entity en = e.getEntity();
        if (! WorldAccess.canPlayerBuild(en.getWorld())) {
            e.blockList().clear();
        }
    }
    
    @EventHandler
    public void blockPlace(BlockPlaceEvent e) {
        if (! WorldAccess.canPlayerBuild(e.getBlock().getWorld())) {
            if (e.getPlayer().isOp())
                return;
            e.setCancelled(true);
        }
    }
    
    @EventHandler
    public void targ(EntityTargetEvent e) {
        if (! WorldAccess.canPlayerBuild(e.getEntity().getWorld())) {
            if (e.getEntity() instanceof Monster && e.getTarget() instanceof Monster) {
                e.setCancelled(true);
            }
        }
    }
    
    @EventHandler
    public void edbe(EntityDamageByEntityEvent e) {
        if (e.getEntity().getWorld() == misc) {
            if (e.getEntity() instanceof Monster) {
                if (e.getDamager() instanceof Arrow)
                    e.setCancelled(!(((Arrow) e.getDamager()).getShooter() instanceof Player));
                else if (e.getDamager() instanceof Projectile)
                    e.setCancelled(!(((Projectile) e.getDamager()).getShooter() instanceof Player));
                else if (e.getDamager() instanceof EvokerFangs)
                    e.setCancelled(true);
            }
        }
    }

}
